# Hello!

Welcome to Agenda.

### Controls

#### Groups and files

To get started, click on the <i class="fa fa-plus"></i> <i class="fa fa-folder-o"></i> button in the sidebar to create a group.

Inside of a group you can create a new file by clicking the <i class="fa fa-plus"></i> <i class="fa fa-file-text-o"></i> button.

You can edit a group name or file name by hovering over the name and clicking on the <i class="fa fa-pencil-square-o"></i> button.

You can also delete the group or file by selecting the <i class="fa fa-trash-o"></i> button.

You can view a file by clicking on the name. This will display the contents.

#### Editing a file

To edit a file, select the file from the sidebar and then select the <i class="fa fa-pencil"></i> button in the top right to enter edit mode.

This opens 2 panels. The Markdown edit panel and the live preview panel. Update the file by editing the Markdown in the markdown edit panel.

To see how to edit Markdown files, [view this link here](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).

Once you are finished, hit the <i class="fa fa-save"></i> button in the top right to exit edit mode and save changes.

To discard changes click on the <i class="fa fa-times"></i> button in the top right.