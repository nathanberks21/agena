# Agenda

This is the base project for the agenda application. The app should be forked so that the output markdown files can be pushed to the forked repo, and data preserved (because the app runs without a database; this is by design due to wanting to run the app on a local filesystem).

## Developer

As the project comes with the frontend prebuilt in the dist folder this will need overwriting, if you wish to make changes to the frontend.

From the root project folder:

`cd app`
`npm i` (if required)
`npm run build`

If you wish to test changes on the frontend you can run watch:

Open two command lines.

Start the server

`cd server`
`npm i` (if required)
`npm start`
Visit URL `localhost:9000`

Build webpack:

`cd ../app`
`npm i` (if required)
`npm run build`

TODO:
Implement hot reloading - Currently the webpage needs to be manually refreshed, and webpack needs to be built every time. `webpack --watch` could partially solve but a better solution would be required to prevent the need to refresh the page when changes are made.

## Starting the Server

From the root project folder:

`cd server`
`npm i` (if required)
`npm start`
Visit URL `localhost:9000`

### Project Contents

The project is separated into four parts.

__/app__
Contains the frontend application built with React. The project includes the frontend pre-built app so it can be readily cloned and spun up without the need to build.

__/server__
Contains the server implementation for serving the frontend and the API.

__/markdown__
Markdown files placed in here are rendered as HTML. They are expected in a specific format described below.

HOME.md - this is rendered when the application first loads and is regarded as the home page.

To render additional pages they should be placed in the following structure `FOLDER_NAME/MARKDOWN.md`

`FOLDER_NAME` is then rendered in the side nav as an accordion element.

Any number of `MARKDOWN.md` contained inside of `FOLDER_NAME` are then rendered as items inside the accordion element, displayed with the .md extension removed. Selecting one of these files in the frontend will render the markdown as HTML in the markdown viewer area.

Ofcourse, each `FOLDER_NAME` needs to have a unique name and each `MARKDOWN.md` needs to have it's own name.

__/resources__

This folder should contain any image resources to display inside your markdown.