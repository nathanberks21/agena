import React from 'react';
import { connect } from 'react-redux';
import { Markdown } from 'react-showdown';

import styles from './Overview.css';

import * as Api from '../../actions/apiActions.js';

import NoFile from '../../components/noFile/NoFile.jsx';
import Editor from '../../components/editor/Editor.jsx';
import ConfirmationDialog from '../../components/confirmationDialog/ConfirmationDialog.jsx';

@connect((store) => {
    return {
        getMarkdown : store.getMarkdown,
        saveFile    : store.saveFile
    }
})
export default class Overview extends React.Component {
    constructor() {
        super();

        this.state = {
            editActive      : false,
            markdown        : '',
            savable         : false,
            editConfirmation: false
        }
    }
    componentWillMount() {
        const {folder, file} = this.props.match.params;
        this.props.dispatch(Api.getFile(folder, file));
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.match.url !== this.props.match.url) {
            const {folder, file} = nextProps.match.params;
            this.props.dispatch(Api.getFile(folder, file));
        }
        if (nextProps.getMarkdown.markdown !== this.props.getMarkdown.markdown) {
            this.setState({'markdown': nextProps.getMarkdown.markdown});
        }
        if (!nextProps.saveFile.saving && this.props.saveFile.saving) {
            const {folder, file} = this.props.match.params;
            this.props.dispatch(Api.getFile(folder, file));
            this.setState({'savable': false});
            this._toggleEditMode();
        }
    };

    render() {
        return (
            <div className={styles.container}>
                {
                    this.props.getMarkdown.error
                    ? <NoFile file={this.props.match.params.file} group={this.props.match.params.folder}/>
                    : (
                        <div className={styles.container}>
                             {
                                this.state.editActive
                                    ? (
                                        <div className={styles.toolbar}>
                                            <div className={styles.flexTool}>Markdown</div>
                                            <div className={styles.flexTool}>Preview
                                                <div className={styles.controls}>
                                                    {
                                                        this.props.saveFile.saving ? <i className={`fa fa-circle-o-notch fa-spin`}></i> : ''
                                                    }
                                                    <i
                                                        className={`fa fa-floppy-o ${styles.button} ${(this.state.savable && !this.props.saveFile.saving) ? '' : styles.disabled}`}
                                                        title="Save and close"
                                                        onClick={this._save.bind(this)}>
                                                    </i>
                                                    <i className={`fa fa-times ${styles.button}`} title="Finish" onClick={this._finishEdit.bind(this)}></i>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                    : (
                                        <div className={styles.toolbar}>
                                            <div className={styles.controls}>
                                                <i className={`fa fa-pencil ${styles.button}`} title="Edit markdown" onClick={this._toggleEditMode.bind(this)}></i>
                                            </div>
                                        </div>
                                    )
                            }
                            <div className={styles.viewContainer}>
                                {
                                    this.state.editActive
                                        ? (
                                        <div className={styles.editor}>
                                            <Editor markdown={this.state.markdown} update={this._updatePreview.bind(this)} />
                                        </div>
                                        )
                                        : ''
                                }
                                <div className={styles.preview}>
                                    <div className={styles.previewWrapper}>
                                        <Markdown markup={this.state.markdown} />
                                    </div>
                                </div>
                            </div>
                             <ConfirmationDialog
                                message={`Exit edit mode without saving changes?`}
                                confirm={() => this._toggleEditMode(true)}
                                cancel={this._cancelFinish.bind(this)}
                                show={this.state.editConfirmation}
                            />
                        </div>
                    )
                }
            </div>
        );
    }

    _updatePreview(value) {
        if (this.props.getMarkdown.markdown !== value) {
            this.setState({'markdown': value, 'savable': true});
        } else {
            this.setState({'markdown': value, 'savable': false});
        }
    }

    _finishEdit() {
        if (this.state.savable) {
            this.setState({'editConfirmation': true});
        } else {
            this._toggleEditMode();
        }
    }

    _cancelFinish() {
        this.setState({'editConfirmation': false});
    }

    _toggleEditMode(discard) {
        const state = !this.state.editActive;
        this.setState({'editActive': state, 'editConfirmation': false, 'savable': false});
        if (discard) {
            this.setState({'markdown': this.props.getMarkdown.markdown});
        }
    }

    _save() {
        const {folder, file} = this.props.match.params;
        this.props.dispatch(Api.saveFile(folder, file, this.state.markdown));
    }
}
