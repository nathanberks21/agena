import React from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';

import styles from './Sidebar.css';

import * as Api from '../../actions/apiActions.js';

import SidebarGroups from '../../components/sidebarGroups/SidebarGroups.jsx';

@connect((store) => {
    return {
        items       : store.items,
        addFolder   : store.addFolder,
        deleteFolder: store.deleteFolder,
        deleteFile  : store.deleteFile,
        addFile     : store.addFile
    }
})
export default class Sidebar extends React.Component {
    constructor() {
        super();
        this.state = {
            activeFile      : ''
        }
    }

    componentWillMount() {
        this.props.dispatch(Api.getItems());
    }

    componentWillReceiveProps(newProps) {
        if (newProps.match.params.file) {
            this.setState({'activeFile': newProps.match.params.file});
        }
        if (newProps.addFolder.needsRefresh && !this.props.addFolder.needsRefresh) {
            this._refresh();
        }
        if ((newProps.deleteFolder.needsRefresh && !this.props.deleteFolder.needsRefresh) || (newProps.deleteFile.needsRefresh && !this.props.deleteFile.needsRefresh)) {
            this._refresh();
        }
        if (newProps.addFile.needsRefresh && !this.props.addFile.needsRefresh) {
            this._refresh();
        }
    }

    render() {
        return (
            <div className={styles.sidebar}>
                <Link as='div' className={styles.logo} to='/' title="Home">AGENDA</Link>
                {
                    Object.keys(this.props.items.values).map(key => {
                        return (
                            <SidebarGroups 
                                key={key}
                                folder={key}
                                items={this.props.items.values[key]}
                                selectedItem={this.state.activeFile}
                                selectedFolder={this.props.match.params.folder}
                                deleteFolder={this._deleteFolder.bind(this)}
                                deleteFile={this._deleteFile.bind(this)}
                                addFile={this._addFile.bind(this)}
                                refresh={this._refresh.bind(this)}/>
                        );
                    })
                }
                <div className={styles.addFolder} onClick={this._addFolder.bind(this)} title="Add group">
                    <i className={`fa fa-plus ${styles.plusIcon}`}></i>
                    <i className={`fa fa-folder-o ${styles.folderIcon}`}></i>
                </div>
            </div>
        );
    }

    _addFile(folder) {
        this.props.dispatch(Api.addFile(folder));
    }

    _addFolder() {
        this.props.dispatch(Api.addFolder());
    }

    _deleteFolder(folder) {
        this.props.dispatch(Api.deleteFolder(folder));
        if (folder === this.props.match.params.folder) {
            this.props.history.push('/');
        }
    }

    _deleteFile(folder, file) {
        this.props.dispatch(Api.deleteFile(folder, file));
        if (folder === this.props.match.params.folder) {
            this.props.history.push('/');
        }
    }

    _refresh() {
        this.props.dispatch(Api.getItems());
    }
}
