import React from 'react';

import styles from './SidebarGroups.css';

import ConfirmationDialog from '../confirmationDialog/ConfirmationDialog.jsx';
import GroupTitle from '../groupTitle/GroupTitle.jsx';
import FileItem from '../fileItem/FileItem.jsx';

export default class SidebarGroups extends React.Component {
    constructor() {
        super();
        this.state = {
            active          : false,
            deletingFolder  : false,
            deletingFile    : false,
            fileToDelete    : ''
        };
    }

    componentWillMount() {
        if (this.props.selectedFolder === this.props.folder) {
            this.setState({'active': true});
        }
    }

    render() {
        return (
            <div className={this.state.active ? styles.active : ''}>
                <GroupTitle
                active={this.state.active}
                folder={this.props.folder}
                setActive={this._setActive.bind(this)}
                delete={this._deleteFolder.bind(this)}
                refresh={this.props.refresh}/>

                <div className={styles.items}>
                    {
                        this.props.items.map(item => {
                            const activeFile = this.props.selectedItem === item && this.props.selectedFolder === this.props.folder;
                            return (
                                <FileItem
                                key={item}
                                file={item}
                                folder={this.props.folder}
                                active={activeFile}
                                delete={this._deleteFile.bind(this)}
                                refresh={this.props.refresh}/>
                                
                            );
                        })
                    }
                    <div className={styles.addFile} onClick={() => this.props.addFile(this.props.folder)} title="Add file">
                        <i className={`fa fa-plus ${styles.plusIcon}`}></i>
                        <i className={`fa fa-file-text-o ${styles.fileIcon}`}></i>
                    </div>
                </div>
                <ConfirmationDialog
                    message={`Are you sure you want to delete the group '${this.props.folder}'? This action is irreversible and will result in the loss of all containing files.`}
                    confirm={this._confirmDeleteFolder.bind(this)}
                    cancel={this._cancelDeleteFolder.bind(this)}
                    show={this.state.deletingFolder}
                />
                <ConfirmationDialog
                    message={`Are you sure you want to delete the file '${this.state.fileToDelete}' from group '${this.props.folder}'? This action is irreversible.`}
                    confirm={this._confirmDeleteFile.bind(this)}
                    cancel={this._cancelDeleteFile.bind(this)}
                    show={this.state.deletingFile}
                />
            </div>
        );
    }

    _setActive(active) {
        this.setState({'active': active});
    }

    _deleteFolder() {
        this.setState({'deletingFolder': true});
    }

    _confirmDeleteFolder() {
        this.setState({'deletingFolder': false});
        this.props.deleteFolder(this.props.folder);
    }

    _cancelDeleteFolder() {
        this.setState({'deletingFolder': false});
    }

    _deleteFile(file) {
        this.setState({'deletingFile': true, 'fileToDelete': file});
    }

    _confirmDeleteFile() {
        this.setState({'deletingFile': false});
        this.props.deleteFile(this.props.folder, this.state.fileToDelete);
    }

    _cancelDeleteFile() {
        this.setState({'deletingFile': false});
    }
}