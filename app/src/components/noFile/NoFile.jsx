import React from 'react';

import styles from './NoFile.css';

export default class NoFile extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <p className={styles.header}>No markdown found</p>
                <p className={styles.subHeader}>For group '{this.props.group}' and file '{this.props.file}'</p>
            </div>
        );
    }
}
