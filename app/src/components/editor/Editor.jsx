import React from 'react';
import AceEditor from 'react-ace';
import brace from 'brace';

import styles from './Editor.css';

import 'brace/mode/markdown';
import 'brace/theme/xcode';

export default class Editor extends React.Component {
    constructor() {
        super();

        this.state = {
            markdown: ''
        }
    }

    componentWillMount() {
        this.setState({'markdown': this.props.markdown});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.markdown !== this.state.markdown) {
            this.setState({'markdown': nextProps.markdown});
        }
    }

    render() {
        return (
            <div className={styles.editContainer}>
            {
                <AceEditor
                    value={this.state.markdown}
                    mode="markdown"
                    theme="xcode"
                    onChange={this._onChange.bind(this)}
                    name="editor"
                    height="100%"
                    width="100%"
                    focus={true}
                    wrapEnabled={true}
                    fontSize={14}
                    editorProps={{$blockScrolling: Infinity}}
                />

            }
            </div>
        );
    }

    _onChange(value) {
        this.props.update(value);
    }
}
