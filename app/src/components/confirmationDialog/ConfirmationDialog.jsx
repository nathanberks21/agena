import React from 'react';

import styles from './ConfirmationDialog.css';

const KEYS = {
    ENTER   : 13,
    ESC     : 27
};

export default class ConfirmationDialog extends React.Component {
    constructor() {
        super();
        this.keyHandler = this.keyHandler.bind(this);
    }

    componentWillReceiveProps(newProps) {
        if (newProps.show && !this.props.show) {
            window.addEventListener('keyup', this.keyHandler);
        } else {
            window.removeEventListener('keyup', this.keyHandler);
        }
    }

    render() {
        return (
            <div className={`${styles.background} ${this.props.show ? styles.visible : ''}`} onClick={this.props.cancel}>
                <div className={styles.container}>
                    <div className={styles.message}>{this.props.message}</div>
                    <div className={styles.controls}>
                        <div className={styles.accept} onClick={this.props.confirm}><i className="fa fa-check"></i></div>
                        <div className={styles.decline} onClick={this.props.cancel}><i className="fa fa-times"></i></div>
                    </div>
                </div>
            </div>
        );
    }

    keyHandler(ev) {
        switch (ev.keyCode) {
            case KEYS.ENTER:
                this.props.confirm()
                break;
            case KEYS.ESC:
                this.props.cancel()
                break;
        }
    }
}
