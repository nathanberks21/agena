import React from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import styles from './FileItem.css';

import * as Api from '../../actions/apiActions.js';

const KEYS = {
    ENTER   : 13,
    ESC     : 27
};

@connect((store) => {
    return {
        editFilename: store.editFilename
    }
})
class FileItem extends React.Component {
    constructor() {
        super();

        this.state = {
            editActive: false
        };
    }

    componentWillMount() {
        this.setState({'file': this.props.file});
    }

    componentWillReceiveProps(newProps) {
        if (newProps.editFilename.folder === this.props.folder
        && newProps.editFilename.file === this.props.file
        && !newProps.editFilename.saving
        && this.props.editFilename.saving
        && !newProps.editFilename.error) {
            this._editSuccess();
        }
    }

    render() {
        return (
            <div>
                <div className={`${this.props.active ? styles.active : ''} ${styles.itemContainer}`}>
                    {
                        this.state.editActive
                            ? (
                                <div className={styles.editContainer}>
                                    <input
                                    autoFocus
                                    onFocus={this._handleFocus}
                                    className={styles.editInput}
                                    value={this.state.file}
                                    onChange={this._setValue.bind(this)}
                                    onKeyUp={this._handleKeyPress.bind(this)}
                                    onBlur={this._edit.bind(this)}/>
                                    {
                                        this.props.editFilename.folder === this.props.folder
                                        && this.props.editFilename.file === this.props.file
                                        && this.props.editFilename.saving
                                            ? <i className={`fa fa-circle-o-notch fa-spin ${styles.editIcon}`}></i>
                                            : ''
                                    }
                                    
                                </div>
                            )
                            : (
                                <div
                                className={styles.link}
                                onClick={this._fileClick.bind(this)}>
                                    {this.props.file}
                                </div>
                            )
                    }
                    <div className={`${styles.controls} ${this.state.editActive ? styles.active : ''}`}>
                        <i className={`fa fa-pencil-square-o ${styles.button} ${styles.editButton}`} onClick={this._toggleEdit.bind(this)} title="Edit group name"></i>
                        <i className={`fa fa-trash-o ${styles.button}`} onClick={() => this.props.delete(this.props.file)} title="Delete group"></i>
                    </div>
                </div>
                {
                    this.props.editFilename.folder === this.props.folder
                    && this.props.editFilename.file === this.props.file
                    && this.props.editFilename.error
                    && this.props.editFilename.error.length > 0
                    && this.state.editActive
                        ? <div className={styles.error}>{this.props.editFilename.error}</div> : ''
                }
            </div>
        );
    }

    _edit() {
        if (this.props.file !== this.state.file) {
            this.props.dispatch(Api.editFilename(this.props.folder, this.props.file, this.state.file));
        } else {
            this.setState({'editActive': false});
        }
    }

    _editSuccess() {
        this.props.refresh();
        this.setState({'editActive': false});
        const url = `/view/${encodeURI(this.props.folder)}/${encodeURI(this.state.file)}`;
        this.props.history.push(url);
    }

    _setValue(ev) {
        this.setState({'file': ev.target.value});
    }

    _handleKeyPress(ev) {
        switch(ev.keyCode) {
            case KEYS.ENTER:
                this._edit();
                break;
            case KEYS.ESC:
                this.setState({'file': this.props.file, 'editActive': false});
                break;
        }
    }

    _handleFocus(ev) {
        ev.target.select();
    }

    _toggleEdit() {
        if (this.state.editActive) {
            this._edit();
        } else {
            this.setState({'editActive': true});
        }
    }

    _fileClick() {
        if (!this.state.editActive) {
            const url = `/view/${encodeURI(this.props.folder)}/${encodeURI(this.props.file)}`;
            this.props.history.push(url);
        }
    }
}

export default withRouter(FileItem);