import React from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';

import styles from './GroupTitle.css';

import * as Api from '../../actions/apiActions.js';

const KEYS = {
    ENTER   : 13,
    ESC     : 27
};

@connect((store) => {
    return {
        editFolder: store.editFolder
    }
})
class GroupTitle extends React.Component {
    constructor() {
        super();

        this.state = {
            editActive: false,
            folder: ''
        };
    }

    componentWillMount() {
        this.setState({'folder': this.props.folder});
    }

    componentWillReceiveProps(newProps) {
        if (newProps.editFolder.folder === this.props.folder
        && !newProps.editFolder.saving
        && this.props.editFolder.saving
        && !newProps.editFolder.error) {
            this._editSuccess();
        }
    }

    render() {
        return (
            <div>
                <div className={styles.titleContainer}>
                    <div 
                    className={styles.title}
                    onClick={this._groupClick.bind(this)}>
                        <i className={`fa ${this.props.active ? 'fa-caret-down' : 'fa-caret-right'} ${styles.caret}`}></i>
                        {
                            this.state.editActive
                                ? (
                                    <div className={styles.editContainer}>
                                        <input
                                        autoFocus
                                        onKeyUp={this._handleKeyPress.bind(this)}
                                        onBlur={this._edit.bind(this)}
                                        onFocus={this._handleFocus}
                                        className={styles.editInput}
                                        value={this.state.folder}
                                        onChange={this._setValue.bind(this)}/>
                                        {
                                            this.props.editFolder.folder === this.props.folder && this.props.editFolder.saving
                                                ? <i className={`fa fa-circle-o-notch fa-spin ${styles.editIcon}`}></i>
                                                : ''
                                        }
                                        
                                    </div>
                                )
                                : (
                                    <div className={styles.folderName}>{this.props.folder}</div>
                                )
                        }
                    </div>
                    <div className={`${styles.controls} ${this.state.editActive ? styles.active : ''}`}>
                        <i className={`fa fa-pencil-square-o ${styles.button} ${styles.editButton}`} onClick={this._toggleEdit.bind(this)} title="Edit group name"></i>
                        <i className={`fa fa-trash-o ${styles.button}`} onClick={this.props.delete} title="Delete group"></i>
                    </div>
                </div>
                {
                    this.props.editFolder.folder === this.props.folder
                    && this.props.editFolder.error
                    && this.props.editFolder.error.length > 0
                    && this.state.editActive
                        ? <div className={styles.error}>{this.props.editFolder.error}</div> : ''
                }
            </div>
        );
    }

    _edit(ev) {
        if (this.props.folder !== this.state.folder) {
            this.props.dispatch(Api.editFolder(this.props.folder, this.state.folder));
        } else {
            this.setState({'editActive': false});
        }
    }

    _editSuccess() {
        const file = this.props.match.params.file;
        this.props.refresh();
        this.setState({'editActive': false});
        const url = `/view/${encodeURI(this.state.folder)}/${encodeURI(file)}`;
        this.props.history.push(url);
    }

    _setValue(ev) {
        this.setState({'folder': ev.target.value});
    }

    _handleKeyPress(ev) {
        switch(ev.keyCode) {
            case KEYS.ENTER:
                this._edit();
                break;
            case KEYS.ESC:
                this.setState({'folder': this.props.folder, 'editActive': false});
                break;
        }
    }

    _handleFocus(ev) {
        ev.target.select();
    }

    _toggleEdit() {
        if (this.state.editActive) {
            this._edit();
        } else {
            this.setState({'editActive': true});
        }
    }

    _groupClick() {
        if (!this.state.editActive) {
            const state = !this.props.active;
            this.props.setActive(state);
        }
    }
}

export default withRouter(GroupTitle);