const DEFAULT = {
    saving      : false,
    error       : '',
    folder      : ''
}

const editFolderReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'EDIT_FOLDER_START': {
            state = Object.assign({}, state, {
                saving  : true,
                error   : '',
                folder  : action.folder
            });
            break;
        }
        case 'EDIT_FOLDER': {
            state = Object.assign({}, state, {
                saving: false,
                folder  : action.folder
            });
            break;
        }
        case 'EDIT_FOLDER_ERROR': {
            state = Object.assign({}, state, {
                saving  : false,
                error   : action.error,
                folder  : action.folder
            });
            break;
        }
    }
    return state;
};

export default editFolderReducer;
