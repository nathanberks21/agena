const DEFAULT = {
    saving      : false,
    error       : '',
    needsRefresh: false
}

const createFileReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'ADD_FILE_START': {
            state = Object.assign({}, state, {
                saving      : true,
                error       : '',
                needsRefresh: false
            });
            break;
        }
        case 'ADD_FILE': {
            state = Object.assign({}, state, {
                saving      : false,
                needsRefresh: true
            });
            break;
        }
        case 'ADD_FILE_ERROR': {
            state = Object.assign({}, state, {
                saving  : false,
                error   : action.error
            });
            break;
        }
    }
    return state;
};

export default createFileReducer;
