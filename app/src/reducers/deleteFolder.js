const DEFAULT = {
    deleting    : false,
    error       : '',
    needsRefresh: false
}

const deleteFolderReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'DELETE_FOLDER_START': {
            state = Object.assign({}, state, {
                deleting    : true,
                error       : '',
                needsRefresh: false
            });
            break;
        }
        case 'DELETE_FOLDER': {
            state = Object.assign({}, state, {
                deleting    : false,
                needsRefresh: true
            });
            break;
        }
        case 'DELETE_FOLDER_ERROR': {
            state = Object.assign({}, state, {
                deleting: false,
                error   : action.error
            });
            break;
        }
    }
    return state;
};

export default deleteFolderReducer;
