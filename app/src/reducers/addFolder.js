const DEFAULT = {
    saving      : false,
    error       : '',
    needsRefresh : false
}

const addFolderReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'ADD_FOLDER_START': {
            state = Object.assign({}, state, {
                saving      : true,
                error       : '',
                needsRefresh: false
            });
            break;
        }
        case 'ADD_FOLDER': {
            state = Object.assign({}, state, {
                saving      : false,
                needsRefresh: true
            });
            break;
        }
        case 'ADD_FOLDER_ERROR': {
            state = Object.assign({}, state, {
                saving  : false,
                error   : action.error
            });
            break;
        }
    }
    return state;
};

export default addFolderReducer;
