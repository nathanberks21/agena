const DEFAULT = {
    deleting    : false,
    error       : '',
    needsRefresh: false
}

const deleteFileReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'DELETE_FILE_START': {
            state = Object.assign({}, state, {
                deleting    : true,
                error       : '',
                needsRefresh: false
            });
            break;
        }
        case 'DELETE_FILE': {
            state = Object.assign({}, state, {
                deleting    : false,
                needsRefresh: true
            });
            break;
        }
        case 'DELETE_FILE_ERROR': {
            state = Object.assign({}, state, {
                deleting: false,
                error   : action.error
            });
            break;
        }
    }
    return state;
};

export default deleteFileReducer;
