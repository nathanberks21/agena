const DEFAULT = {
    loading: false,
    fileError  : false,
    markdown: ''
}

const fileReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'GET_FILE_START': {
            state = Object.assign({}, state, {
                loading : true,
                error   : false
            });
            break;
        }
        case 'GET_FILE': {
            state = Object.assign({}, state, {
                markdown: action.markdown,
                loading : false
            });
            break;
        }
        case 'GET_FILE_ERROR': {
            state = Object.assign({}, state, {
                loading : false,
                error   : action.error
            });
            break;
        }
    }
    return state;
};

export default fileReducer;
