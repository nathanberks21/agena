const DEFAULT = {
    saving  : false,
    error   : '',
    folder  : '',
    file    : ''
}

const editFilenameReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'EDIT_FILENAME_START': {
            state = Object.assign({}, state, {
                saving  : true,
                error   : '',
                folder  : action.folder,
                file    : action.file
            });
            break;
        }
        case 'EDIT_FILENAME': {
            state = Object.assign({}, state, {
                saving  : false,
                folder  : action.folder,
                file    : action.file
            });
            break;
        }
        case 'EDIT_FILENAME_ERROR': {
            state = Object.assign({}, state, {
                saving  : false,
                error   : action.error,
                folder  : action.folder,
                file    : action.file
            });
            break;
        }
    }
    return state;
};

export default editFilenameReducer;
