import { combineReducers } from 'redux';

import itemsReducer from './items.js';
import getMarkdownReducer from './getMarkdown.js';
import addFolderReducer from './addFolder.js';
import editFolderReducer from './editFolder.js';
import deleteFolderReducer from './deleteFolder.js';
import addFileReducer from './addFile.js';
import editFileReducer from './editFilename.js';
import deleteFileReducer from './deleteFile.js';
import saveFileReducer from './saveFile.js';

export default combineReducers({
    items       : itemsReducer,
    getMarkdown : getMarkdownReducer,
    addFolder   : addFolderReducer,
    deleteFolder: deleteFolderReducer,
    addFile     : addFileReducer,
    deleteFile  : deleteFileReducer,
    editFolder  : editFolderReducer,
    editFilename: editFileReducer,
    saveFile    : saveFileReducer
});
