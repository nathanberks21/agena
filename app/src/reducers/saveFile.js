const DEFAULT = {
    saving  : false,
    error   : '',
    markdown: ''
}

const editFilenameReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'SAVE_FILE_START': {
            state = Object.assign({}, state, {
                saving  : true,
                error   : ''
            });
            break;
        }
        case 'SAVE_FILE': {
            state = Object.assign({}, state, {
                saving  : false
            });
            break;
        }
        case 'SAVE_FILE_ERROR': {
            state = Object.assign({}, state, {
                saving  : false,
                error   : action.error
            });
            break;
        }
    }
    return state;
};

export default editFilenameReducer;
