const DEFAULT = {
    loadingItems: false,
    itemsError  : false,
    values       : [],
}

const sortByDate = (a, b) => {
    const dA = new Date(a);
    const dB = new Date(b);
    return dA < dB;
}

const itemReducer = function(state=DEFAULT, action) {
    switch (action.type) {
        case 'GET_ITEMS_START': {
            state = Object.assign({}, state, {loadingItems: true});
            break;
        }
        case 'GET_ITEMS': {
            let values = {};
            Object.keys(action.data).sort(sortByDate).forEach(key => values[key] = action.data[key]);
            state = Object.assign({}, state, {
                values,
                loadingItems : false,
                itemsError: false
            });
            break;
        }
        case 'GET_ITEMS_ERROR': {
            state = Object.assign({}, state, {loadingItems: false, itemsError: true});
            break;
        }
    }
    return state;
};

export default itemReducer;
