import 'font-awesome-webpack';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom'

import store from './store.js';

import Overview from './scenes/overview/Overview.jsx';
import Sidebar from './scenes/sidebar/Sidebar.jsx';

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <div className="container">
                <Route exact path="/view/:folder/:file" component={Sidebar}/>
                <Route exact path="/" component={Sidebar}/>
                <Route exact path="/view/:folder/:file" component={Overview}/>
                <Route exact path="/" component={Overview}/>
            </div>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);
