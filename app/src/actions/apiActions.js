import axios from 'axios';

const GET_ITEMS = '/api/items';
const GET_MARKDOWN = '/api/markdown';
const ADD_FOLDER = '/api/folder/add';
const EDIT_FOLDER = '/api/folder/edit';
const DELETE_FOLDER = '/api/folder/delete';
const ADD_FILE = '/api/file/add';
const EDIT_FILENAME = '/api/file/edit';
const DELETE_FILE = '/api/file/delete';
const SAVE_FILE = '/api/file/save';

export function getItems() {
    return function(dispatch) {
        dispatch({type: 'GET_ITEMS_START'});
        axios.get(GET_ITEMS)
            .then(({data}) => {
                dispatch({type: 'GET_ITEMS', data});
            })
            .catch(() => dispatch({type: 'GET_ITEMS_ERROR'}))
    }
}

export function getFile(folder, file) {
    return function(dispatch) {
        dispatch({type: 'GET_FILE_START'});
        const call = file && folder
            ? `${GET_MARKDOWN}/${folder}/${file}`
            : GET_MARKDOWN
        ;
        axios.get(call)
            .then(({data}) => {
                if (!data.success) {
                    return dispatch({type: 'GET_FILE_ERROR', error: data.error});
                }
                dispatch({type: 'GET_FILE', markdown: data.markdown});
            })
            .catch(() => dispatch({type: 'GET_FILE_ERROR'}))
    }
}

export function addFile(folder) {
    return function(dispatch) {
        dispatch({type: 'ADD_FILE_START'});
        axios.post(ADD_FILE, {folder})
            .then(({data}) => {
                if (!data.success) {
                    dispatch({type: 'ADD_FILE_ERROR', error: data.error});
                } else {
                    dispatch({type: 'ADD_FILE'});
                }
            })
            .catch(() => dispatch({type: 'ADD_FILE_ERROR', error: 'An error occurred'}));
    }
}

export function editFilename(folder, file, newName) {
    return function(dispatch) {
        dispatch({type: 'EDIT_FILENAME_START', folder, file});
        if (newName.length === 0) {
            return dispatch({type: 'EDIT_FILENAME_ERROR', error: 'You need to provide a name', folder, file});
        }
        axios.post(EDIT_FILENAME, {folder, file, rename: newName})
            .then(({data}) => {
                if (!data.success) {
                    dispatch({type: 'EDIT_FILENAME_ERROR', error: data.error, folder, file});
                } else {
                    dispatch({type: 'EDIT_FILENAME', folder, file});
                }
            })
            .catch(() => dispatch({type: 'EDIT_FILENAME_ERROR', error: 'An error occurred', folder, file}));
    }
}

export function deleteFile(folder, file) {
    return function(dispatch) {
        dispatch({type: 'DELETE_FILE_START'});
        axios.post(DELETE_FILE, {folder, file})
            .then(({data}) => {
                if (!data.success) {
                    dispatch({type: 'DELETE_FILE_ERROR', error: data.error});
                } else {
                    dispatch({type: 'DELETE_FILE'});
                }
            })
            .catch(() => dispatch({type: 'DELETE_FILE_ERROR', error: 'An error occurred'}));
    }
}

export function saveFile(folder, file, markdown) {
    return function(dispatch) {
        dispatch({type: 'SAVE_FILE_START'});
        let values = {markdown};
        if (folder && file) {
            values = Object.assign(values, {folder, file});
        }
        axios.post(SAVE_FILE, values)
            .then(({data}) => {
                if (!data.success) {
                    dispatch({type: 'SAVE_FILE_ERROR', error: data.error});
                } else {
                    dispatch({type: 'SAVE_FILE'});
                }
            })
            .catch(() => dispatch({type: 'SAVE_FILE_ERROR', error: 'An error occurred'}));
    }
}

export function addFolder() {
    return function(dispatch) {
        dispatch({type: 'ADD_FOLDER_START'});
        axios.post(ADD_FOLDER)
            .then(({data}) => {
                if (!data.success) {
                    dispatch({type: 'ADD_FOLDER_ERROR', error: data.error});
                } else {
                    dispatch({type: 'ADD_FOLDER'});
                }
            })
            .catch(() => dispatch({type: 'ADD_FOLDER_ERROR', error: 'An error occurred'}));
    }
}

export function editFolder(folder, newName) {
    return function(dispatch) {
        dispatch({type: 'EDIT_FOLDER_START', folder});
        if (newName.length === 0) {
            return dispatch({type: 'EDIT_FOLDER_ERROR', error: 'You need to provide a name', folder});
        }
        axios.post(EDIT_FOLDER, {folder, rename: newName})
            .then(({data}) => {
                if (!data.success) {
                    dispatch({type: 'EDIT_FOLDER_ERROR', error: data.error, folder});
                } else {
                    dispatch({type: 'EDIT_FOLDER', folder});
                }
            })
            .catch(() => dispatch({type: 'EDIT_FOLDER_ERROR', error: 'An error occurred', folder}));
    }
}

export function deleteFolder(folder) {
    return function(dispatch) {
        dispatch({type: 'DELETE_FOLDER_START'});
        axios.post(DELETE_FOLDER, {folder})
            .then(({data}) => {
                if (!data.success) {
                    dispatch({type: 'DELETE_FOLDER_ERROR', error: data.error});
                } else {
                    dispatch({type: 'DELETE_FOLDER'});
                }
            })
            .catch(() => dispatch({type: 'DELETE_FOLDER_ERROR', error: 'An error occurred'}));
    }
}