const path = require('path');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const Routes = require('./routes');
const Api = require('./api');
const PORT = process.env.PORT || 9000;

app.use(cors());
app.use(bodyParser());
// React frontend
app.use(express.static(path.resolve(__dirname, '..', 'app', 'dist')));
// Resources folder
app.use(express.static(path.resolve(__dirname, '..', 'resources')));
// Initialise API specific routes
new Api(app);
// Initialise all other routes
new Routes(app);


app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});