const path = require('path');
const { lstatSync, readdirSync, readFileSync, existsSync, mkdirSync, removeSync, writeFileSync, renameSync } = require('fs-extra');
const showdown = require('showdown');
const validFilename = require('valid-filename');
const markdownConverter = new showdown.Converter();

const markdownFolder = path.resolve(__dirname, '..', 'markdown');

// Helper functions
const isDirectory = (source) => lstatSync(source).isDirectory();
const getFolders = () => readdirSync(markdownFolder).filter(name => isDirectory(path.resolve(markdownFolder, name)));
const getItems = (folder) => readdirSync(path.resolve(markdownFolder, folder))
    .filter(name => name.includes('.md'))
    .map(name => name.replace('.md', '')
);

class Api {
    constructor(app) {
        app.get('/api/items', this._getItems);
        app.get('/api/markdown', this._getMarkdown);
        app.get('/api/markdown/:folder/:file', this._getMarkdown);
        app.post('/api/folder/add', this._newFolder);
        app.post('/api/folder/edit', this._editFolder);
        app.post('/api/folder/delete', this._deleteFolder);
        app.post('/api/file/add', this._newFile);
        app.post('/api/file/edit', this._editFile);
        app.post('/api/file/delete', this._deleteFile);
        app.post('/api/file/save', this._saveFile);
    }

    _getItems(req, res) {
        const items = {};
        const folderNames = getFolders();
        folderNames.forEach(folder => items[folder] = getItems(folder));
        res.json(items);
    }

    _getMarkdown(req, res) {
        let file;
        if (req.params.file && req.params.folder) {
            file = path.resolve(markdownFolder, req.params.folder, `${req.params.file}.md`);
        } else {
            file = path.resolve(markdownFolder, 'HOME.md');
        }
        const exists = existsSync(file);
        if (exists) {
            const markdown = readFileSync(file, 'utf8');
            res.json({success: true, error: null, markdown});
        } else {
            res.json({success: false, error: 'No such file'});
        }
    }

    _newFolder(req, res) {
        const groupMatch = /Untitled Group\W\d+/;
        let highestNumber = 0;
        const groupNames = getFolders();
        groupNames
            .filter(group => groupMatch.test(group))
            .forEach(group => {
                group = group.match(groupMatch)[0];
                const number = Number(group.match(/\d+/)[0]);
                highestNumber = number > highestNumber ? number : highestNumber;
            })
        ;
        const folder = `Untitled Group ${highestNumber + 1}`;
        const dir = path.resolve(markdownFolder, folder);
        const exists = existsSync(dir);
        if (!exists) {
            mkdirSync(dir);
            res.json({success: true, error: null});
        } else {
            res.json({success: false, error: 'Folder already exists'});
        }
    }

    _editFolder(req, res) {
        const folder = req.body.folder;
        const newName = req.body.rename;
        if (!validFilename(newName)) {
            return res.json({success: false, error: 'Group name is invalid', folder});
        }
        const dir = path.resolve(markdownFolder, folder);
        const newDir = path.resolve(markdownFolder, newName);
        const exists = existsSync(newDir);
        if (!exists) {
            renameSync(dir, newDir);
            res.json({success: true, error: null, folder});
        } else {
            res.json({success: false, error: 'Group name must be unique', folder});
        }
    }

    _deleteFolder(req, res) {
        const folder = req.body.folder;
        const dir = path.resolve(markdownFolder, folder);
        const exists = existsSync(dir);
        if (exists) {
            removeSync(dir);
            res.json({success: true, error: null});
        } else {
            res.json({success: false, error: 'Folder does not exist'});
        }
    }

    _newFile(req, res) {
        const folder = req.body.folder;
        const fileMatch = /Untitled File\W\d+/;
        let highestNumber = 0;
        const files = getItems(folder);
        files
            .filter(file => fileMatch.test(file))
            .forEach(file => {
                file = file.match(fileMatch)[0];
                const number = Number(file.match(/\d+/)[0]);
                highestNumber = number > highestNumber ? number : highestNumber;
            })
        ;
        const filename = `Untitled File ${highestNumber + 1}`;
        const file = path.resolve(markdownFolder, folder, `${filename}.md`);
        const exists = existsSync(file);
        if (!exists) {
            const data = `# ${filename}`;
            writeFileSync(file, data);
            res.json({success: true, error: null});
        } else {
            res.json({success: false, error: 'File with name already exists'});
        }
    }

    _editFile(req, res) {
        const folder = req.body.folder;
        const file = req.body.file;
        const newName = req.body.rename;
        if (!validFilename(newName)) {
            return res.json({success: false, error: 'File name is invalid', folder, file});
        }
        const dir = path.resolve(markdownFolder, folder, `${file}.md`);
        const newDir = path.resolve(markdownFolder, folder, `${newName}.md`);
        const exists = existsSync(newDir);
        if (!exists) {
            renameSync(dir, newDir);
            res.json({success: true, error: null, folder, file});
        } else {
            res.json({success: false, error: 'File name must be unique', folder, file});
        }
    }

    _deleteFile(req, res) {
        const folder = req.body.folder;
        const file = req.body.file;
        const dir = path.resolve(markdownFolder, folder, `${file}.md`);
        const exists = existsSync(dir);
        if (exists) {
            removeSync(dir);
            res.json({success: true, error: null});
        } else {
            res.json({success: false, error: 'File does not exist'});
        }
    }

    _saveFile(req, res) {
        const folder = req.body.folder;
        const filename = req.body.file;
        const markdown = req.body.markdown;
        let file;
        if (folder && filename) {
            file = path.resolve(markdownFolder, folder, `${filename}.md`);
        } else {
            file = path.resolve(markdownFolder, 'HOME.md');
        }
        const exists = existsSync(file);
        if (exists) {
            writeFileSync(file, markdown);
            res.json({success: true, error: null});
        } else {
            res.json({success: false, error: 'File to edit does not exist'});
        }
    }    
}

module.exports = Api;