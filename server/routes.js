const path = require('path');

class Routes {
    constructor(app) {
        app.get('*', (req, res) => {
            res.sendFile(path.resolve(__dirname, '..', 'app', 'dist', 'index.html'));
        });
    }
}

module.exports = Routes;